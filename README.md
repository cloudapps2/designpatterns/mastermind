<h1 align="center">Welcome to Master Mind
 👋</h1>
<p>
  <a href="https://gitlab.com/cloudapps2/designpatterns/mastermind/-/tree/mvp.pm.withComposite" target="_blank">
    COMPOSITE
  </a>
  <a href="https://gitlab.com/cloudapps2/designpatterns/mastermind/-/tree/mvp.pm.withProxy" target="_blank">
    PROXY
  </a>
</p>

<h1> EVALUACIÓN </h1>

**TOTAL HORAS TRABAJADAS**
- Viernes 23 (mvp.pm.withComposite) —> Clases + 2 horas de práctica
- Sábado 24 (mvp.pm.withComposite) —> 6 horas
- Domingo 25 (mvp.pm.withComposite) —> 7 horas
- Lunes 26 (mvp.pm.withComposite) —> 3 horas
- Martes 27 (mvp.pm.withComposite) —> Clases
- Miércoles 28 (mvp.pm.withComposite) —> Clases + 1 hora
- Jueves 29 (mvp.pm.withProxy) —> 3 horas
- Viernes 30 (mvp.pm.withProxy) —> Clases
- Sábado 31 (mvp.pm.withProxy) —> 6 horas
- Domingo 1 (Diagramas) —> 9 horas
- Total —> **37 horas** de práctica.

Me costó bastante coger el ritmo de las clases y las prácticas. Cada vez que se hablaba de algo me sentía bastante atrasado con respecto a mis compañeros. 
- Verme todos los vídeos de nuevo para entender los conceptos
- Comparar código desde la primera rama para entenderlo en profundidad
- Realizar todos los diagramas. Para ello tuve que ver funcionamiento PUML y ponerme con ello. Me llevó más tiempo del que hubiera querido


Por todo ello considero que **mi nota sería un 5**
- Me falta la última parte y algo del diseño
- El esfuerzo realizado ha sido enorme
- Espero ya estar al 100% para la próxima práctica.

