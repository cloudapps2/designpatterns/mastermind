package usantatecla.mastermind.views;

import usantatecla.mastermind.controllers.AcceptorController;

public abstract class View{
	
	public abstract void interact(AcceptorController accceptorController);

}
