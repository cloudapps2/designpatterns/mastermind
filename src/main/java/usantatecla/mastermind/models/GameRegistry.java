package usantatecla.mastermind.models;

import java.util.ArrayList;
import java.util.List;

public class GameRegistry {

	private List<GameMemento> memento;
	
	private Game game;
	
	private int firstPrevious;

	public GameRegistry(Game game) {
		this.game = game;
		this.reset();
	}
	
	void reset() {
		this.memento = new ArrayList<>();
		this.firstPrevious = 0;
	}
}
