package usantatecla.mastermind.controllers;

import usantatecla.mastermind.models.Game;
import usantatecla.mastermind.models.State;

public class PlayController extends Controller implements AcceptorController{
	
	private ProposalController proposalController;
	
	private UndoController undoController;

	PlayController(Game game, State state) {
		super(game, state);
		
		this.proposalController = new ProposalController(game, state);
	}

	@Override
	public void accept(ControllersVisitor controllersVisitor) {
		controllersVisitor.visit(this);
	}

}
